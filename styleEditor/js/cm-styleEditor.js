
(function ($) {
    /**
     * 默认参数
     */
    var defaults = {
        stage: '#mainDiv', //舞台
        resizeItemClass: 'resize-item', //可缩放的类名
        selectedItem: null,
        styleList: [],
        keys: {
            'ArrowUp': 38,
            'ArrowDown': 40,
            'ArrowLeft': 37,
            'ArrowRight': 39,
        },
    };
    /**
     * 定义类
     */
    var StyleEditor = function (options) {
        this.options = $.extend({}, defaults, options);
        this.init();
    }

    StyleEditor.prototype = {
        init: function () {
            var self = this;
            $(document).createGrid({
                appendId: "mainDiv",
                containerId: "containerId",
                containerWidth: "1024",
                containerHeight: "768",
                containerResizeScale: "1",
                containerGridWidthHeight: "180"
            });
            self.initResize();
            var i = 1;
            $('.style-tool').draggable({
                proxy: 'clone',
                revert: true,
                onStopDrag: function (e) {
                    var target = e.target;
                    if (target && !target.classList.contains('stage')) {
                        return;
                    }
                    var p = $(this).clone();
                    self.options.styleList.push(p)
                    var left = e.offsetX + 'px';
                    var top = e.offsetY + 'px'
                    p.css({
                        position: 'absolute',
                        left: left,
                        top: top,
                        zindex: i + 1
                    });
                    var oid = "imageDiv" + (i++);
                    p.attr("id", oid);
                    p.addClass('resize-item');
                    p.appendTo('.stage');
                    p.draggable({
                        onDrag: function (e) {
                            var d = e.data;
                            if (d.left < 0) { d.left = 0 }
                            if (d.top < 0) { d.top = 0 }
                            if (d.left + $(d.target).outerWidth() > $(d.parent).width()) {
                                d.left = $(d.parent).width() - $(d.target).outerWidth();
                            }
                            if (d.top + $(d.target).outerHeight() > $(d.parent).height()) {
                                d.top = $(d.parent).height() - $(d.target).outerHeight();
                            }
                        }
                    });
                    p.resizable({
                        onStopResize: function (e) {
                            var stage = $(self.options.stage).find('.stage');
                            var maxWidth = stage.width();
                            var maxHeight = stage.height();
                            var data = e.data;
                            var width = data.left + data.width;
                            var height = data.top + data.height;
                            if (data.left < 0) {
                                $(this).css({
                                    left: 0
                                });
                                $(this).width(data.startLeft + data.startWidth);
                            } else if (width > maxWidth) {
                                $(this).width(maxWidth - data.left);
                            }
                            if (data.top < 0) {
                                $(this).css({
                                    top: 0
                                });
                                $(this).height(data.startTop + data.startHeight);
                            } else if (height > maxHeight) {
                                $(this).height(maxHeight - data.top);
                            }

                        }
                    });
                    self.initResizeSingle(p);
                }
            });
            self.bindEvent();
        },

        initResize: function () {
            var self = this;
            $('.' + self.options.itemClass).each(function () {
                self.initResizeSingle($(this))
            });
        },

        initResizeSingle: function ($item) {
            var self = this;
            var obj = $item;
            if (obj.data('cm-resize')) {
                return;
            }
            obj.data('cm-resize', true);
            var width = obj.width();
            var height = obj.height();
            var resizePanel = $('<div class="resize-panel"></div>');
            resizePanel.css({
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                position: 'absolute',
                'background-color': 'rgba(0,0,0,0.5)',
                cursor: 'move',
                display: 'none'
            });
            self.appendTo(resizePanel, obj);
            var n = $('<div class="n"></div>');//北
            var s = $('<div class="s"></div>');//南
            var w = $('<div class="w"></div>');//西
            var e = $('<div class="e"></div>');//东
            var ne = $('<div class="ne"></div>');//东北
            var nw = $('<div class="nw"></div>');//西北
            var se = $('<div class="se"></div>');//东南
            var sw = $('<div class="sw"></div>');//西南
            self.addHandlerCss([n, s, w, e, ne, nw, se, sw]);
            self.appendTo([n, s, w, e, ne, nw, se, sw], resizePanel);
        },

        bindEvent: function () {
            var self = this;
            self.bindSelectToolEvent();
            self.bindKeyPressEvent();
            self.bindMenuEvent();
        },
        //控制点公共样式
        addHandlerCss: function (els) {
            for (var i = 0; i < els.length; i++) {
                el = els[i];
                el.css({
                    position: 'absolute',
                    width: '8px',
                    height: '8px',
                    background: '#ff6600',
                    'border-radius': '2px',
                    border: '1px solid #dd5500',
                });
            }
        },

        appendTo: function (handlers, target) {
            for (var i = 0; i < handlers.length; i++) {
                el = handlers[i];
                target.append(el);
            }
        },

        bindSelectToolEvent: function () {
            var self = this;
            $(self.options.stage).on('click', function (e) {
                var resizeItem = $('.' + self.options.resizeItemClass);
                if (e.target.classList.contains(self.options.resizeItemClass)) {
                    resizeItem.removeClass('style-selected');
                    $(e.target).addClass('style-selected');
                    self.options.selectedItem = e.target;
                } else {
                    resizeItem.removeClass('style-selected');
                    self.options.selectedItem = null;
                }
            })

        },

        bindKeyPressEvent: function () {
            var self = this;
            $(document).keydown(function (event) {
                var selectedItem = $(self.options.selectedItem);
                if (event.keyCode == 46 && selectedItem) {
                    $.messager.confirm('确认对话框', '您想要删除吗？', function (r) {
                        if (r) {
                            self.deleteStyleItem(selectedItem)
                        }
                    });

                }
                if (selectedItem) {
                    var position = selectedItem.position();
                    var parent = selectedItem.parent('div');
                    var maxLeft = parent.innerWidth() - selectedItem.width();
                    var maxTop = parent.innerHeight() - selectedItem.height();
                    if (event.keyCode == self.options.keys['ArrowUp']) {
                        var newTop = position.top - 1;
                        if (newTop < 0) {
                            newTop = 0
                        }
                        selectedItem.css({ top: newTop })
                    }
                    if (event.keyCode == self.options.keys['ArrowDown']) {
                        var newTop = position.top + 1;
                        if (newTop > maxTop) {
                            newTop = maxTop
                        }
                        selectedItem.css({ top: newTop })
                    }
                    if (event.keyCode == self.options.keys['ArrowLeft']) {
                        var newLeft = position.left - 1;
                        if (newLeft < 0) {
                            newLeft = 0;
                        }
                        selectedItem.css({ left: newLeft })
                    }
                    if (event.keyCode == self.options.keys['ArrowRight']) {
                        var newLeft = position.left + 1;
                        if (newLeft > maxLeft) {
                            newLeft = maxLeft;
                        }
                        selectedItem.css({ left: newLeft })
                    }
                }
            });
        },

        bindMenuEvent: function () {
            var self = this;
            $('#style-menu').menu({
                onClick: function (item) {
                    log('menu onClick', item);
                    if (item.text == '水平居中') {
                        self.horizontalCenterStyle();
                    }
                    if (item.text == '垂直居中') {
                        self.verticalCenterStyle();
                    }
                    if(item.text == '居中'){
                        self.centerStyle();
                    }
                    if (item.text == '铺满全屏') {
                        self.fullScreenStyle();
                    }
                    
                }

            })
            $('#mainDiv').on('contextmenu', function (e) {
                e.preventDefault();
                self.showMenu(e.clientX, e.clientY);
            })
        },

        showMenu: function (x, y) {
            $('#style-menu').menu('show', {
                left: x,
                top: y
            });
        },
        deleteStyleItem: function (el) {
            var self = this;
            self.options.styleList = _.remove(self.options.styleList, function (n) {
                if (n == self.options.selectedItem) {
                    return true;
                }
            });
            if (self.options.selectedItem) {
                $(self.options.selectedItem).remove();
                self.options.selectedItem = null;
            }
        },

        horizontalCenterStyle: function () {
            var self = this;

            if (self.options.selectedItem) {
                var selectedItem = $(self.options.selectedItem);
                var parent = selectedItem.parent('div');
                var left = (parent.innerWidth() - selectedItem.width()) / 2;
                // var top = (parent.innerHeight() - selectedItem.height()) / 2;
                selectedItem.css({
                    left: left
                });
                log('horizontalCenter', parent, left)
            }
        },
        verticalCenterStyle: function () {
            var self = this;
            if (self.options.selectedItem) {
                var selectedItem = $(self.options.selectedItem);
                var parent = selectedItem.parent('div');
                // var left = (parent.innerWidth() - selectedItem.width()) / 2;
                var top = (parent.innerHeight() - selectedItem.height()) / 2;
                selectedItem.css({
                    top: top
                });
            }
        },
        centerStyle:function() {
            var self = this;
            if (self.options.selectedItem) {
                var selectedItem = $(self.options.selectedItem);
                var parent = selectedItem.parent('div');
                var left = (parent.innerWidth() - selectedItem.width()) / 2;
                var top = (parent.innerHeight() - selectedItem.height()) / 2;
                selectedItem.css({
                    top: top,
                    left: left
                });
            }
        },
        fullScreenStyle: function() {
            var self = this;
            if (self.options.selectedItem) {
                var selectedItem = $(self.options.selectedItem);
                var parent = selectedItem.parent('div');
                var left = 0;
                var top = 0;
                selectedItem.css({
                    top: top,
                    left: left,
                    width: parent.width(),
                    height:parent.height(),
                }); 
            } 
        },
        getOptions: function () {
            return this.options;
        }
    }

    window.StyleEditor = StyleEditor;
})(jQuery);