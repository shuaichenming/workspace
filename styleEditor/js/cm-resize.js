; (function ($) {
    var defaults = {
        stage: document, //舞台
        itemClass: 'resize-item', //可缩放的类名
    };

    var CMResize = function (options) {
        this.options = $.extend({}, defaults, options);
        this.init();
    }

    CMResize.prototype = {
        init: function () {
            var self = this;
            $('.' + self.options.itemClass).each(function () {
                if ($(this).data('cm-resize')) {
                    return;
                }
                $(this).data('cm-resize', true);
                var width = $(this).width();
                var height = $(this).height();
                var resizePanel = $('<div class"resize-panel"></div>');
                resizePanel.css({
                    width: width,
                    height: height,
                    top: 0,
                    left: 0,
                    position: 'absolute',
                    'background-color': 'rgba(0,0,0,0.5)',
                    cursor: 'move',
                    display: 'none'
                });
                self.appendPoint(resizePanel, $(this));
                /**
                 * 创建控制点
                 */
                var n = $('<div class="n"></div>');//北
                var s = $('<div class="s"></div>');//南
                var w = $('<div class="w"></div>');//西
                var e = $('<div class="e"></div>');//东
                var ne = $('<div class="ne"></div>');//东北
                var nw = $('<div class="nw"></div>');//西北
                var se = $('<div class="se"></div>');//东南
                var sw = $('<div class="sw"></div>');//西南

                //添加公共样式
                // self.addPointBaseCss([n, s, w, e, ne, nw, se, sw]);

            });

        },
        //控制点公共样式
        addPointBaseCss: function (els) {
            for (var i = 0; i < els.length; i++) {
                el = els[i];
                el.css({
                    position: 'absolute',
                    width: '8px',
                    height: '8px',
                    background: '#ff6600',
                    margin: '0',
                    'border-radius': '2px',
                    border: '1px solid #dd5500',
                });
            }
        },
        appendPoint: function (handlers, target) {
            for (var i = 0; i < handlers.length; i++) {
                el = handlers[i];
                target.append(el);
            }
        },
        triggerResize: function (el) {
            var self = this;
            el.siblings('.' + self.options.itemClass).children('div').css({
                display: 'none'
            });
            el.children('div').css({
                display: 'block'
            });
        },
    }
})