(function($) {
	$.fn.createGrid = function(options) {
		var defaults = {
			appendId : options.appendId,
			containerId : options.containerId,
			containerWidth : options.containerWidth,
			containerHeight : options.containerHeight,
			containerResizeScale : options.containerResizeScale,
			containerGridWidthHeight : options.containerGridWidthHeight
		};
		var options = $.extend(defaults, options);
		if(options.appendId == null) {
			return;
		}
		if(options.containerResizeScale == null) {
			options.containerResizeScale = 1.0;
		}
		if(options.containerWidth == null) {
			options.containerWidth = innerWidth;
		}
		if(options.containerHeight == null) {
			options.containerHeight = innerHeight;
		}
		if(options.containerGridWidthHeight == null) {
			options.containerGridWidthHeight = 180;
		}
		if(options.containerId == null) {
			options.containerId = Math.random();
		}
		//e0e0e0
		var containerDiv = $('<div class="stage" style="border: 1px solid #99bbe8;position: relative;"></div>');;
		containerDiv.attr('id',options.containerId);
		var w = (options.containerWidth * options.containerResizeScale).toFixed(4);
		var h = (options.containerHeight * options.containerResizeScale).toFixed(4);
		var r = (options.containerGridWidthHeight * options.containerResizeScale).toFixed(4);
		containerDiv.css("width",w);
		containerDiv.css("height",h);
		var hc = Math.floor(h/r);
		var wc = Math.floor(w/r);
		var ld;
		for(var i = 1; i <= wc; i++) {
			ld = $('<div style="background-color: #e0e0e0;position: absolute;width:1px;"></div>');
			ld.css("height",h);
			ld.css("left",i*r);
			containerDiv.append(ld);
		}
		for(var i = 1; i <= hc; i++) {
			ld = $('<div style="background-color: #e0e0e0;position: absolute;height:1px;"></div>');
			ld.css("width",w);
			ld.css("top",i*r);
			containerDiv.append(ld);
		}
		$("#" + options.appendId).append(containerDiv);
		return containerDiv;
	};
})(jQuery);